import colorama
from colorama import Fore, Style

printInColor = True

def getGreenText(text, shouldPrint=True):
    """
    print text in green
    """
    if not printInColor:
        print(text)
        return

    greenText = Fore.GREEN + text + Style.RESET_ALL
    if shouldPrint:
        print(greenText)

    return greenText
    #print(f"{Fore.GREEN}Heeloo{Style.RESET_ALL}")

def getRedText(text, shouldPrint=True):
    """
    print text in red
    """
    if not printInColor:
        print(text)
        return

    redText = Fore.RED + text + Style.RESET_ALL
    if shouldPrint:
        print(redText)

    return redText


def readPolicies(policyText):
    """
    Read the policies variable containing list of policies
    """
    policies = policyText.strip().split("\n")

    for policy in policies:
        policySequenceInText(policy)

def policySequenceInText(policyLine):
    """
    Of the form 'XXXX\trequired_approval'
    """
    policySequence, approvalRequired = policyLine.split("\t")
    approvalRequired = approvalRequired.replace("required", "require")
    humanReadableForm = "Policy where request is of form: "

    level = 0
    for i in policySequence:
        if i == "?":
            stateText = "unspecified"
        elif i=='X':
            stateText = "doesn't matter"
        else:
            stateText = levelToTextMap[level][i]
    
        humanReadableForm += levelToTextMap[level]["levelName"] + " is " + stateText + (" AND " if (level+1 != len(policySequence)) else " WILL ")

        level += 1

    approvalRequired = approvalRequired.replace("_", " ")
    humanReadableForm +=  approvalRequired
    print(humanReadableForm)
    return humanReadableForm

def branchSequenceInText(branchSequence):
    """
    Convert it in human readable form
    """
    humanReadableForm = ""
    level = 0
    for i in branchSequence:
        if i == "?":
            stateText = "unspecified"
        else:
            stateText = levelToTextMap[level][i]

        humanReadableForm += levelToTextMap[level]["levelName"] + " is " + stateText + (" AND " if (level+1 != len(branchSequence)) else "")

        level += 1

    return humanReadableForm

policies = """
????	required_approval
110X	not_required_approval
111X	required_approval
00XX	not_required_approval
01XX	required_approval
0?XX	required_approval
10XX	not_required_approval
1?XX	required_approval
11?X	required_approval
?XXX	required_approval
"""

levelToTextMap = {
    0 : { "levelName" : "resource",
            "0"       : "sensitive",
            "1"       : "nonsensitive" },
    1 : { "levelName" : "environment",
            "0"       : "staging",
            "1"       : "production" },
    2 : { "levelName" : "access level",
            "0"       : "read only",
            "1"       : "write or admin" },
    3 : { "levelName" : "time required",
            "0"       : "short-term",
            "1"       : "long-term"     },
}

readPolicies(policies)

policyMap = {}
policies = policies.strip().split("\n")

for policy in policies:
    policy_  = policy.split("\t")
    policyMap[policy_[0].strip()] = policy_[1].strip()

#print(policyMap)

def xnor(symbol1, symbol2):
    if (symbol1 == "X") or (symbol2 == "X"):
        return True

    if (symbol1 == symbol2):
        return True

    return False


def evaluatePolicy(policySequence, branchSequence):
    if (len(policySequence) != len(branchSequence)):
        print("Issue with branch sequence %s : %s"%(policySequence, branchSequence))
        return

    sequenceLength  = len(policySequence)
    doesPolicyMatch = True
    for i in range(sequenceLength):
        doesPolicyMatch = doesPolicyMatch and xnor(policySequence[i], branchSequence[i])
        
    return doesPolicyMatch


ALPHABET = \
  "0123456789abcdefghijklmnopqrstuvwxyz"

def encode(x, symbolString):
    """
    x is a decimal number
    """
    try:
        return symbolString[x]
    except IndexError:
        raise Exception("Cannot encode %s"%x)

def decimal2terinary(x, symbolString):
    """
    x is decimal number
    """
    base = len(symbolString)
    if x < base:
        return encode(x, symbolString)

    else:
        return decimal2terinary(x // base, symbolString) + \
                    encode(x % base, symbolString)

def generateBranch(bits, symbolString):
    generatedBranchSequence = []
    base = len(symbolString)
    decimalRange = base ** bits

    for i in range(decimalRange):
        convertedNumber = decimal2terinary(i, symbolString)
        convertedString = str(convertedNumber)
        # Padding 0s
        lengthString = len(convertedString)
        if lengthString < bits:
            convertedString = (bits-lengthString) * "0" + convertedString

        #print("=> ", convertedString)
        generatedBranchSequence.append(convertedString)

    return generatedBranchSequence

branchSequences = generateBranch(4, "01?")


policySequences = policyMap.keys()
### Test if all the policy maps are comprehensive
allBranchesComputed = True
for branchSequence in branchSequences:
    policyMatched = False
    for policySequence in policySequences:
        policyMatched = policyMatched or evaluatePolicy(policySequence, branchSequence)


    if not policyMatched:
        allBranchesComputed = False
        #print("No policy matched for branch sequence: ", branchSequence)
        getRedText("No policy found for case where " + branchSequenceInText(branchSequence) + ": %s"%branchSequence  + "\n")

if allBranchesComputed:
    getGreenText("The policies cover all the case scenarios")
