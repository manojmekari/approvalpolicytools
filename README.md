This tool is used for Talenta's Access Request Approval Framework

Simply check the variable _policies_ to add/remove new access policy and then run:

```
$ python3 comprehensivePolicies.py
```

This will throw out any case which all the policies may not cover.
